/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Ezekiel
 */
public class SqlQueries {
    
    public void addStudent(int id, String fName, String lName, int admyr,
    double gpa, String prog){
            Connection conn = MySqlConnect.ConnectDB();
    ResultSet result = null;
    PreparedStatement statement = null;
    
            String sql = "INSERT INTO studdata VALUES(?,?,?,?,?,?)";
        try {
            statement = conn.prepareStatement(sql);
            statement.setString(1, String.valueOf(id));
            statement.setString(2, String.valueOf(fName));
            statement.setString(3, String.valueOf(lName));
            statement.setString(4, String.valueOf(admyr));
            statement.setString(5, String.valueOf(gpa));
            statement.setString(6, String.valueOf(prog));
            statement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Student added successfuly");
        }
        catch (SQLException | HeadlessException e){
            JOptionPane.showMessageDialog(null, e.getMessage());
            //return null;
        }
    }
    
    public void updateStudent(int id, String fName, String lName, int admyr,
    double gpa, String prog){
                    Connection conn = MySqlConnect.ConnectDB();
    ResultSet result = null;
    PreparedStatement statement = null;
    
            String sql = "UPDATE studdata SET firstName = ?, lastName = ?, "
                    + "admYr = ?,GPA = ?, program = ? WHERE studentID = ?";
        try {
            statement = conn.prepareStatement(sql);
            statement.setString(1, String.valueOf(fName));
            statement.setString(2, String.valueOf(lName));
            statement.setString(3, String.valueOf(admyr));
            statement.setString(4, String.valueOf(gpa));
            statement.setString(5, String.valueOf(prog));
            statement.setString(6, String.valueOf(id));
            statement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Student updated successfuly");
        }
        catch (SQLException | HeadlessException e){
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    
    public void deleteStudent(int id){
            Connection conn = MySqlConnect.ConnectDB();
    ResultSet result = null;
    PreparedStatement statement = null;
    
            String sql = "DELETE FROM studdata WHERE studentID = ?";
        try {
            statement = conn.prepareStatement(sql);
            statement.setString(1, String.valueOf(id));
             statement.executeUpdate();
              JOptionPane.showMessageDialog(null, "Student deleted successfuly");
        }
        catch (Exception e){
            JOptionPane.showMessageDialog(null, e.getMessage());

        }
    }
    
    public static ResultSet displayAllStudent(){
    Connection conn = MySqlConnect.ConnectDB();
    ResultSet result = null;
    PreparedStatement statement = null;
    
            String sql = "select * from studdata";
        try {
            statement = conn.prepareStatement(sql);
             result = statement.executeQuery();

             return result;
        }
        catch (Exception e){
            JOptionPane.showMessageDialog(null, e.getMessage());
            return null;
        }
    }
    
    public ResultSet getItem(String id){
        
        Connection conn = MySqlConnect.ConnectDB();
        ResultSet result = null;
        PreparedStatement statement = null;
    
            String sql = "SELECT * FROM studdata WHERE studentID = ?";
        try {
            statement = conn.prepareStatement(sql);
            statement.setString(1, id);
             result = statement.executeQuery();

             return result;
        }
        catch (Exception e){
            return null;
        }
    }

}
